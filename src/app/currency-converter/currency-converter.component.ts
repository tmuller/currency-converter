import { Component, OnInit } from '@angular/core';
import {ExchangeRateService} from '../services/exchange-rate.service';

@Component({
  selector: 'app-currency-converter',
  templateUrl: './currency-converter.component.html',
  styleUrls: ['./currency-converter.component.sass']
})
export class CurrencyConverterComponent implements OnInit {

  public sourceCurrencies = ['USD', 'EUR', 'AUD', 'CAD'];
  public sourceAmount: number;
  public sourceCurrency: string;
  public exchangeRates = [];
  public targetCurrency: string;
  public calculatedValueForeignCurrency: number;

  constructor(private exchangeService: ExchangeRateService) { }

  ngOnInit() {
    this.sourceCurrency = this.sourceCurrencies[0];
    this.loadCurrencyConversionFor(this.sourceCurrency);
  }

  private loadCurrencyConversionFor(currency: string): Promise<any> {
    return this.exchangeService.getCurrencyConversionRatesFor(currency)
      .then(exchangeRateInfo => {
        this.exchangeRates = exchangeRateInfo;
        console.log(this.exchangeRates);
        this.targetCurrency = this.exchangeRates[0].abbr;
      });
  }

  public onUserLeaveValue($event): void {
    this.sourceAmount = parseInt(($event.target as HTMLInputElement).value, 10);
    this.convertOriginAmount();
  }

  public onSourceCurrencySelected($event): void {
    this.sourceCurrency = ($event.target as HTMLInputElement).value;
    this.loadCurrencyConversionFor(this.sourceCurrency)
      .then(this.convertOriginAmount.bind(this));
  }

  public onTargetCurrencyChange(selectedCurrency: string): void {
    this.targetCurrency = selectedCurrency;
    this.convertOriginAmount();
  }

  public convertOriginAmount() {
    const exchangeRate = this.exchangeRates.filter( r => r.abbr === this.targetCurrency)[0].rate;
    this.calculatedValueForeignCurrency = this.sourceAmount * exchangeRate;
  }

}
