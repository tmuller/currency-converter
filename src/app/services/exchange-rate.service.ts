import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ExchangeRateService {

  private sourceURL = ' https://api.exchangerate-api.com/v4/latest';

  // public getCurrencyConversionRatesFor(currency: string): Promise<any> {
  //   return this.http.get<any>(`${this.sourceURL}/${currency}`)
  //     .toPromise();
  // }
  private currencies = ['USD', 'EUR', 'AUD', 'CAD', 'AED', 'DKK'];

  constructor() {}

  public getCurrencyConversionRatesFor(currency: string): Promise<any> {

    return new Promise((resolve, reject) => {

      const rates = this.generateExchangeRatesFor(currency);
      console.log('rates', rates);
      setTimeout(() => {
        resolve(rates);
      }, 1000);
    });
  }

  private generateExchangeRatesFor(sourceCurrency: string): object {

    const rateAnswer = {
      base: sourceCurrency,
      date: '2019-10-14',
      time_last_updated: 1571011896,
      rates: []
    };

    for (const currency of this.currencies) {
      if (currency === sourceCurrency) {
        rateAnswer.rates.push({
          abbr: currency,
          rate: 1,
        });
      } else {
        rateAnswer.rates.push({
          abbr: currency,
          rate: Math.trunc(Math.random() * 100000000) / 1000000,
        });
      }

    }
    return rateAnswer.rates;
  }

}
