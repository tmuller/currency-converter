import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-currency-list',
  templateUrl: './currency-list.component.html',
  styleUrls: ['./currency-list.component.sass']
})
export class CurrencyListComponent {

  @Input()
  public allExchangeRates;

  @Output()
  public currencySelectedEmitter = new EventEmitter<any>();

  public onCurrencySelected(currencyInfo): void {
    this.currencySelectedEmitter.emit(currencyInfo.abbr);
  }
}
